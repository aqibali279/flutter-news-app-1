import 'package:flutter/material.dart';
import 'screens/launch.dart';
import 'utlis/constants.dart';
import 'network/user_service.dart';
import 'network/categories_service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

//  @overrideR
  Widget build(BuildContext context) {

    _getParentCategories();
    _getUserInfo();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Constants.APP_NAME,
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: LaunchScreen(),
    );
  }

   _getUserInfo() async {
    await UserService.instance.user();
  }

  _getParentCategories()async{
    await CategoriesService.instance.loadCategories(0);
  }


}



