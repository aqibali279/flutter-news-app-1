import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ShareUtil{

  static var instance = ShareUtil._instance();

  ShareUtil._instance();

  share(BuildContext context,int postId){
    switch(Theme.of(context).platform){
      case TargetPlatform.iOS:
        Share.share('com.tos.reader://tos/post?id=$postId');
        break;
      case TargetPlatform.android:
        Share.share('www.tos.com?postId=$postId');
        break;
      case TargetPlatform.fuchsia:
        break;
    }
  }

}