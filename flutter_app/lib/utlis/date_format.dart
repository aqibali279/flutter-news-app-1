
import 'package:date_format/date_format.dart';

class DateFormat{

  static var instance = DateFormat._instance();

  DateFormat._instance();

  String format(String dateString){
    var dateTime = DateTime.parse(dateString).toLocal();
    var str = formatDate(dateTime, ['hh', ':', 'nn', ' ', 'am', ' ', 'D', ' ', 'M', ' ', 'yy']);
    return str;
  }

}