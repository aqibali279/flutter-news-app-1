import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app/network/user_service.dart';

class Preferences{

  static var instance = Preferences._instance();
  final String _COOKIE = 'cookie';

  Preferences._instance();

  Future<SharedPreferences> _get()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  setCookie(String cookie)async{
    var prefs = await _get();
    prefs.setString(_COOKIE, cookie);
  }

  Future<String> getCookie()async{
    var prefs = await _get();
    return prefs.getString(_COOKIE);
  }


  Future<bool> removeData() async {
    var prefs = await _get();
    UserService.instance.authCookie.user = null;
    return prefs.remove(_COOKIE);
  }


}