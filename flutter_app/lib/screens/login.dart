import 'package:flutter/material.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/screens/sign_up.dart';
import 'package:flutter_app/custom_widgets/text_field.dart';
import 'package:flutter_app/network/authentication_service.dart';
import 'dart:async';
import 'package:flutter_app/network/user_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  var usernameNode = FocusNode();
  var passwordNode = FocusNode();
  var usernameController = TextEditingController();
  var passwordController = TextEditingController();
  String usernameError, passwordError;
  var _key = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        appBar: AppBar(
          title: Text(Constants.LOGIN,
              style: TextStyle(fontWeight: FontWeight.w400)),
        ),
        body: GestureDetector(
          child: ListView(
            padding: EdgeInsets.all(20),
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: Text(
                    Constants.WELCOME,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                    textAlign: TextAlign.center,
                  )),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: CustomTextField(
                    context,
                    Constants.USERNAME_HINT,
                    Constants.USERNAME,
                    usernameError,
                    Icons.account_circle,
                    usernameController,
                    focusNode: usernameNode,
                    nextFocusNode: passwordNode,
                    onChanged: (text) {
                      setState(() {
                        usernameError = null;
                      });
                    },
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                child: CustomTextField(
                  context,
                  Constants.PASSWORD_HINT,
                  Constants.PASSWORD,
                  passwordError,
                  Icons.lock,
                  passwordController,
                  focusNode: passwordNode,
                  nextFocusNode: FocusNode(),
                  onChanged: (text) {
                    setState(() {
                      passwordError = null;
                    });
                  },
                ),
              ),
              Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: RaisedButton(
                    color: Colors.blue,
                    textColor: Colors.white,
                    child: Text(Constants.LOGIN),
                    onPressed: () async {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          });
                      var flag = await _login();
                      Navigator.pop(context);
                      if (flag) {
                        _showSnackBar(Constants.LOGIN_SUCCESS);
                        Future.delayed(Duration(seconds: 2), () {
                          Navigator.pop(context);
                        });
                      }
                    },
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        Constants.REGISTER,
                        style: TextStyle(fontWeight: FontWeight.w300),
                        textAlign: TextAlign.right,
                      ),
                    ),
                    Expanded(
                      child: GestureDetector(
                        child: Text(
                          Constants.SIGN_UP,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.blue),
                          textAlign: TextAlign.left,
                        ),
                        onTap: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUpScreen()));
                        },
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
        ));
  }

  Future<bool> _login() async {
    var username = usernameController.text;
    var password = passwordController.text;

    if (username.isEmpty) {
      setState(() {
        usernameError = Constants.USERNAME_ERROR;
      });
    }

    if (password.isEmpty) {
      setState(() {
        passwordError = Constants.PASSWORD_ERROR;
      });
    }

    if (username.isEmpty || password.isEmpty) {
      return false;
    }

    var authCookie = await AuthenticationService.instance.requestLogin(username, password);
    if (authCookie.error == null) {
      return true;
    } else {
      _showSnackBar(authCookie.error);
      return false;
    }
  }

  _showSnackBar(String message) {
    var snackBar =
        SnackBar(content: Text(message), duration: Duration(seconds: 1));
    _key.currentState.showSnackBar(snackBar);
  }
}
