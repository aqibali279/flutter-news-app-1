import 'package:flutter/material.dart';
import 'package:flutter_app/models/categories.dart';
import 'package:flutter_app/network/categories_service.dart';
import 'package:flutter_app/network/posts_service.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/utlis/preferences.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/screens/login.dart';
import 'package:flutter_app/cells/home_item.dart';
import 'package:flutter_app/network/user_service.dart';
import "package:pull_to_refresh/pull_to_refresh.dart";
import 'package:flutter_app/screens/more_screen.dart';
import 'package:flutter_app/cells/more_item.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  List<Category> parentCategories;

  var parentCategoriesCount = 0;
  var title = '';
  Widget bodyWidget;
  Widget drawerHeader;
  var menuItem = Constants.LOGIN;
  var _refreshController = RefreshController();
  Category _selectedCategory;
  var _isFirstRequest = true;

  @override
  void initState() {
    super.initState();

    parentCategories = CategoriesService.instance.parentCategories;
    parentCategoriesCount = parentCategories.length;
    title = parentCategories.first.name;
    _selectedCategory = parentCategories.first;
    _isFirstRequest = true;
    bodyWidget = _getListView(parentCategories.first.id);
  }

  @override
  Widget build(BuildContext context) {
    _setDrawerHeader();
    _setMenuItem();
    return Scaffold(
        appBar: AppBar(
          title: Text(title, style: TextStyle(fontWeight: FontWeight.w400)),
          actions: <Widget>[_menu()],
        ),
        body: bodyWidget,
        drawer: Drawer(
            child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            drawerHeader,
            ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 20),
              itemBuilder: (context, index) {
                var category = parentCategories[index];
                return ListTile(
                  title: Text(category.name),
                  onTap: () {
                    setState(() {
                      _selectedCategory = category;
                      _isFirstRequest = true;
                      bodyWidget = category.id == 0
                          ? _savedPosts(category)
                          : _getListView(category.id);
                      title = category.name;
                      Navigator.pop(context);
                    });
                  },
                );
              },
              itemCount: parentCategoriesCount,
            ),
          ],
        )));
  }

  _setDrawerHeader() {
    var user = UserService.instance.authCookie.user;
    if (user != null) {
      drawerHeader = DrawerHeader(
          decoration: BoxDecoration(
              gradient:
                  LinearGradient(colors: [Colors.blue, Colors.blueAccent])),
          child: Center(
            child: ListTile(
              contentPadding: EdgeInsets.all(0),
              title: Text(
                user.displayname,
                style: TextStyle(color: Colors.white),
              ),
              subtitle: Text(
                user.email,
                style: TextStyle(color: Colors.white),
              ),
              leading: Container(
                  width: 80.0,
                  height: 80.0,
                  decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.cover,
                          image: new NetworkImage(
                              'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png')))),
            ),
          ));
    } else {
      drawerHeader = Container();
    }
  }



  FutureBuilder<List<Category>> _getListView(int id) {
    return FutureBuilder<List<Category>>(
        future: CategoriesService.instance.loadCategories(id),
        builder:
            (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            if(!_isFirstRequest)
              _refreshController.sendBack(true, RefreshStatus.completed);
            var categories = snapshot.data;
            return SmartRefresher(
                controller: _refreshController,
                enablePullDown: true,
                onRefresh: (bool) {
                  _isFirstRequest = false;
                  bodyWidget = _getListView(_selectedCategory.id);
                },
                child: ListView.builder(
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 4,
                      margin: EdgeInsets.fromLTRB(6, 3, 6, 3),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: ListView(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          children: <Widget>[
                            ListTile(
                              title: ListTile(
                                contentPadding: EdgeInsets.all(0),
                                leading: Text(
                                  categories[index].name,
                                  style: TextStyle(
                                      fontSize: 12, fontWeight: FontWeight.bold),
                                ),
                                trailing: GestureDetector(
                                  onTap: (){
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => MoreScreen(categories[index])));
                                  },
                                  child: Text(
                                    Constants.MORE,
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontSize: 12
                                    ),
                                    textAlign: TextAlign.end,
                                  ),
                                )
                              ),
                            ),
                            _getPosts(categories[index])
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: categories.length,
                )
            );
          }

          if(!_isFirstRequest && snapshot.error != null)
            _refreshController.sendBack(true, RefreshStatus.failed);

          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  PopupMenuButton _menu() {
    var menu = PopupMenuButton<String>(
      icon: Icon(Icons.more_vert),
      itemBuilder: (BuildContext context) {
        return [menuItem].map((String choice) {
          return PopupMenuItem<String>(
            value: choice,
            child: Text(choice),
          );
        }).toList();
      },
      onSelected: (item) {
        switch (item) {
          case Constants.LOGOUT:
            showDialog<void>(
              context: context, // user must tap button!
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(Constants.LOGOUT_MESSAGE),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(Constants.NO),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    FlatButton(
                      child: Text(Constants.YES),
                      onPressed: () {
                        Preferences.instance.removeData();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
            break;
          case Constants.LOGIN:
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
            break;
        }
      },
    );

    return menu;
  }

  FutureBuilder<List<Post>> _getPosts(Category category) {

    var isHorizontal = category.name.toLowerCase() == 'photos' || category.name.toLowerCase() == 'videos';

    return FutureBuilder<List<Post>>(
      future: PostsService.instance.loadPosts(category.id,10,1),
      builder: (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          var posts = snapshot.data;
          if (posts.length > 0){
            return
              Container(
                padding: EdgeInsets.all(0),
              margin: EdgeInsets.all(0),
              height: isHorizontal ? 120 : null,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: posts.length,
                physics:  isHorizontal ? ClampingScrollPhysics() : NeverScrollableScrollPhysics(),
                scrollDirection: isHorizontal ? Axis.horizontal : Axis.vertical,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(0),
                    child:  isHorizontal ? Container(
                      margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                      width: 120,
                      child: MoreItem(posts[index]),
                    ) : HomeItem(posts[index],category),
                  );
                },
              ),
            );
          }else{
            return Center(
              child: Text(Constants.NO_POSTS),
            );
          }
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  FutureBuilder<List<Post>> _savedPosts(Category category) {
    return FutureBuilder<List<Post>>(
      future: PostsService.instance.getSavedPosts(),
      builder: (BuildContext context, AsyncSnapshot<List<Post>> snapshot) {
        if (snapshot.hasData && snapshot.data != null) {
          var posts = snapshot.data;
          if (posts.length > 0){
            return Card(
                elevation: 4,
                margin: EdgeInsets.fromLTRB(6, 3, 6, 3),
                child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: posts.length,
                      itemBuilder: (context, index) {
                        return Container(
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: HomeItem(posts[index],category),
                        );
                      },
                    )));
          }else{
            return Center(
              child: Text(Constants.NO_POSTS),
            );
          }
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  _setMenuItem() async {
    var cookie = await Preferences.instance.getCookie();
    setState(() {
      menuItem = cookie == null ? Constants.LOGIN : Constants.LOGOUT;
    });
  }
}
