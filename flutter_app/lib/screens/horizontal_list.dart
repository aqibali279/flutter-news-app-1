import 'package:flutter/material.dart';


class HorizontalListView extends StatefulWidget{
  @override
  HorizontalListViewState createState() => HorizontalListViewState();
}

class HorizontalListViewState extends State<HorizontalListView>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('List'),),
      body:  ListView(
        // This next line does the trick.
        scrollDirection: Axis.vertical,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
            height: 200,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  width: 160.0,
                  height: 160.0,
                  color: Colors.blue,
                ),
                Container(
                  width: 160.0,
                  height: 160.0,
                  color: Colors.red,
                ),Container(
                  width: 160.0,
                  height: 160.0,
                  color: Colors.indigo,
                )
                ,Container(
                  width: 160.0,
                  height: 160.0,
                  color: Colors.lightGreen,
                ),Container(
                  width: 160.0,
                  height: 160.0,
                  color: Colors.deepOrange,
                )
              ],
            ),
          ),
          Container(
            width: 160.0,
            height: 160.0,
            color: Colors.blue,
          ),
          Container(
            width: 160.0,
            height: 160.0,
            color: Colors.green,
          ),
          Container(
            width: 160.0,
            height: 160.0,
            color: Colors.yellow,
          ),
          Container(
            width: 160.0,
            height: 160.0,
            color: Colors.orange,
          ),
        ],
      ),
    );
  }

}