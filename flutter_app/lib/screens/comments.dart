import 'package:flutter/material.dart';
import 'package:flutter_app/models/comments.dart';
import 'package:flutter_app/network/comment_service.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/utlis/html.dart';
import 'package:flutter_app/utlis/date_format.dart';
import 'package:flutter_app/screens/add_comment.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/screens/login.dart';
import 'package:flutter_app/network/authentication_service.dart';

class CommentsScreen extends StatefulWidget {
  final Post post;

  CommentsScreen(this.post);

  @override
  CommentsScreenState createState() => CommentsScreenState(post);
}

class CommentsScreenState extends State<CommentsScreen>  {

  Post post;
  bool _isCookieValid = false;

  CommentsScreenState(this.post);


  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    _validateCookie();

    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.COMMENTS,
            style: TextStyle(fontWeight: FontWeight.w400)),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            _push(context,
                _isCookieValid ? AddCommentScreen(post) : LoginScreen());
          }),
      body: FutureBuilder<List<Comment>>(
        future: CommentService.instance.getComments(post.id),
        builder: (BuildContext context, AsyncSnapshot<List<Comment>> snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            var comments = snapshot.data;
            return ListView.builder(
                itemCount: comments.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 4,
                    margin: EdgeInsets.fromLTRB(6, 3, 6, 3),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: Image.network(
                            comments[index].authorUrl.thumbnail,
                            width: 30,
                            height: 30,
                          ),
                          title: Text(
                            comments[index].authorName,
                            style: TextStyle(fontSize: 12,fontWeight: FontWeight.w700),
                          ),
                          subtitle: Text(
                            DateFormat.instance.format(comments[index].date),
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            Html.removeTags(
                                comments[index].content.textString.trim()),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: TextStyle(fontSize: 12),
                          ),
                        ),
                      ],
                    ),
                  );
                });
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }

  _validateCookie() async {
    var status = await AuthenticationService.instance.validate();
    _isCookieValid = status.valid;
  }

  _push(BuildContext context, StatefulWidget widget) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => widget));
  }
}
