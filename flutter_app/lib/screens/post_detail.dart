import 'package:flutter/material.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter_app/screens/comments.dart';
import 'package:flutter_app/utlis/database_helper.dart';
import 'package:flutter_app/utlis/share.dart';

class PostDetailScreen extends StatefulWidget {
  final Post _post;
  final String url;
  final bool isAd;
  PostDetailScreen(this._post, this.url, this.isAd);

  @override
  PostDetailScreenState createState() => PostDetailScreenState(_post,url,isAd);
}

class PostDetailScreenState extends State<PostDetailScreen> {
  Post _post;
  String _url;
  bool isAd;
  var _startIcon = Icons.star_border;
  var _key = GlobalKey<ScaffoldState>();

  PostDetailScreenState(this._post,this._url,this.isAd);

  @override
  Widget build(BuildContext context) {

    _setStar();

    return WebviewScaffold(
      key: _key,
      appBar: AppBar(
        title: Text(''),
        actions:  _getActions(),
      ),
      url: _url,
      withJavascript: true,
    );
  }

  _starTapped() async {
    var saved = await DatabaseHelper().isPostSaved(_post);
    var result = saved ? await DatabaseHelper().deletePost(_post) : await DatabaseHelper().insertPost(_post);

//    var snackBar = SnackBar(content: Text(saved ? Constants.REMOVED : Constants.ADDED),duration: Duration(seconds: 1),);
//    _key.currentState.showSnackBar(snackBar);

    _setStar();
  }


    _getActions(){

    if (isAd){
      return null;
    }

    return [GestureDetector(
      child: Icon(Icons.share),
      onTap: () {
        ShareUtil.instance.share(context, _post.id);
      },
    ),
    Container(width: 15),
    GestureDetector(
      child: Icon(_startIcon),
      onTap: () {
        _starTapped();
      },
    ),
    Container(width: 15),
    GestureDetector(
        child: Icon(Icons.comment),
        onTap: () {
          Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => CommentsScreen(_post)));
        }),
    Container(width: 15),];

  }


  _setStar() async {
    var isSaved = await DatabaseHelper().isPostSaved(_post);
    setState(() {
      _startIcon = isSaved ? Icons.star : Icons.star_border ;
    });
  }
}
