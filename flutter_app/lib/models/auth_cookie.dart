import 'package:flutter_app/models/status.dart';

class AuthCookie extends Status {
  String cookie;
  String cookie_name;
  User user;

  AuthCookie(status, this.cookie, this.cookie_name, error, this.user)
      : super(status, error);

  factory AuthCookie.fromJson(Map<String, dynamic> json) {
    return AuthCookie(
      json['status'],
      json['cookie'],
      json['cookie_name'],
      json['error'],
      User.fromJson(json['user']),
    );
  }
}



class User {
  int id;
  String username;
  String nicename;
  String email;
  String url;
  String registered;
  String displayname;
  String firstname;
  String lastname;
  String nickname;
  String description;

  User(
      this.id,
      this.username,
      this.nicename,
      this.email,
      this.url,
      this.registered,
      this.displayname,
      this.firstname,
      this.lastname,
      this.nickname,
      this.description);

  factory User.fromJson(Map<String, dynamic> json) {
    if (json == null){
      return null;
    }
    return User(
        json['id'],
        json['username'],
        json['nicename'],
        json['email'],
        json['url'],
        json['registered'],
        json['displayname'],
        json['firstname'],
        json['lastname'],
        json['nickname'],
        json['description']);
  }
}
