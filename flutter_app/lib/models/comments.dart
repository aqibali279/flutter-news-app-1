import 'package:flutter_app/models/rendered.dart';


class CommentList {
  List<Comment> comments;

  CommentList(this.comments);

  factory CommentList.fromJson(List<dynamic> parsedJson) {
    var comments = List<Comment>();

    for (var item in parsedJson) {
      comments.add(Comment.fromJson(item));
    }

    return CommentList(comments);
  }
}

class Comment {
  int id;
  String date;
  String authorName;
  Rendered content;
  AuthorUrl authorUrl;

  Comment(this.id,this.date, this.authorName,this.content, this.authorUrl);

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
        json['id'],
        json['date'],
        json['author_name'],
        Rendered.fromJson(json['content']),
        AuthorUrl.fromJson(json['author_avatar_urls'])
    );
  }
}

class AuthorUrl {
  String thumbnail;

  AuthorUrl(this.thumbnail);

  factory AuthorUrl.fromJson(Map<String, dynamic> json) {
    return AuthorUrl(json['24']);
  }
}
