class Nonce {
  String status;
  String controller;
  String method;
  String nonce;
  String error;

  Nonce(this.status, this.controller, this.method, this.nonce,this.error);

  factory Nonce.fromJson(Map<String,dynamic> json) {
    return Nonce(
        json['status'],
        json['controller'],
        json['method'],
        json['nonce'],
        json['error']
    );
  }
}
