class Status {
  String status;
  String error;
  bool valid;

  Status(this.status, this.error, {this.valid});

  factory Status.fromJson(Map<String, dynamic> json) {
    return Status(
        json['status'],
        json['error'],
        valid: json['valid']
    );
  }
}
