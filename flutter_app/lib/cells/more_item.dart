import 'package:flutter/material.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/custom_widgets/pop_menu.dart';
import 'package:flutter_app/screens/post_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';


class MoreItem extends StatefulWidget{

  final Post _post;

  MoreItem(this._post);

  @override
  MoreItemState createState() => MoreItemState(_post);

}

class MoreItemState extends State<MoreItem> {

  Post _post;

  MoreItemState(this._post);

  @override
  Widget build(BuildContext context) {

    return Stack(
      fit: StackFit.passthrough,
      alignment:  Alignment.center,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        PostDetailScreen(_post,_post.link,false)));
          },
          child: Container(
            margin: EdgeInsets.all(1),
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Center(
                    child: Container(
                      constraints: BoxConstraints.expand(),
                      child:
                      CachedNetworkImage(
                        imageUrl: _post.imageUrl,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => new CircularProgressIndicator(),
                        errorWidget: (context, url, error) => new Icon(Icons.error),
                      )
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    _post.title,
                    style: TextStyle(fontSize: 12),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
        ),


        Positioned(
            top: -5,
            right:-10,
            child: PopUpMenu(_post,Colors.white)
        )
      ],
    );
  }



}